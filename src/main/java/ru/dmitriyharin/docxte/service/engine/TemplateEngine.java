package ru.dmitriyharin.docxte.service.engine;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.exception.engine.EngineException;

import java.io.InputStream;
import java.io.OutputStream;

public interface TemplateEngine {
    void process(InputStream source, OutputStream out, Context context) throws EngineException;
}
