package ru.dmitriyharin.docxte.service.engine.interpreter.language;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;

public interface LanguageInterpreter<T extends LanguageItem> {
    Object run(T item, ParseContext parseContext, Context context);
}
