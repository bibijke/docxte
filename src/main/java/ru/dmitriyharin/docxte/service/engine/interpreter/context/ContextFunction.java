package ru.dmitriyharin.docxte.service.engine.interpreter.context;

import java.util.List;

public interface ContextFunction {
    String getName();

    Object execute(List<Object> arguments);
}
