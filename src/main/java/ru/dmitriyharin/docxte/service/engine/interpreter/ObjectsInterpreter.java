package ru.dmitriyharin.docxte.service.engine.interpreter;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;

import java.util.List;

public interface ObjectsInterpreter {
    List<Object> run(List<LanguageItem> languageItems, ParseContext parseContext, Context context);
}
