package ru.dmitriyharin.docxte.service.engine.interpreter.language.instruction;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.LanguageInterpreter;

import java.util.List;

public interface InstructionInterpreter<T extends Instruction> extends LanguageInterpreter<T> {
   List<Object> run(T instruction, ParseContext parseContext, Context context);
}
