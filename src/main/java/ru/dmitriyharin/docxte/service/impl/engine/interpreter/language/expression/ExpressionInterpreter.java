package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.LanguageInterpreter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.ObjectUtils.isEmpty;

public abstract class ExpressionInterpreter<T extends Expression> implements LanguageInterpreter<T> {
    public <R> R run(T expr, Class<R> tClass, ParseContext parseContext, Context context) {
        return tClass.cast(run(expr, parseContext, context));
    }

    protected List<Object> evaluate(List<? extends Expression> expressions, ParseContext parseContext, Context context) {
        if (isEmpty(expressions)) {
            return Collections.emptyList();
        }

        return expressions
            .stream()
            .map(expr -> ExpressionInterpreterFactory
                .get(expr)
                .run(expr, parseContext, context))
            .collect(Collectors.toList());
    }
}
