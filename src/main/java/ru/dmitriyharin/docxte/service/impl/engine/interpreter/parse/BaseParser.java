package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import ru.ahml.apxd.antlr.DocxTELexer;
import ru.ahml.apxd.antlr.DocxTEParser;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.Template;
import ru.dmitriyharin.docxte.service.impl.engine.antlr.visitor.TemplateVisitor;

import static java.lang.String.format;

public abstract class BaseParser<T> {
    public abstract ParseResult parse(T input);

    protected Template parse(String input) {
        final DocxTELexer lexer = new DocxTELexer(CharStreams.fromString(input));

        final CommonTokenStream tokens = new CommonTokenStream(lexer);

        final DocxTEParser parser = new DocxTEParser(tokens);

        final TemplateVisitor visitor = new TemplateVisitor();

        final LanguageItem item = visitor.visit(parser.template());

        if (!(item instanceof Template)) {
            throw new IllegalArgumentException(format("Expected template, but %s given", item.getClass()));
        }

        return (Template) item;
    }
}
