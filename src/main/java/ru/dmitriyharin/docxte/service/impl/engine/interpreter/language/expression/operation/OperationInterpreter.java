package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.operation;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.Operation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;

import java.math.BigDecimal;
import java.util.List;

import static java.lang.String.format;
import static java.math.RoundingMode.HALF_UP;
import static java.util.stream.Collectors.joining;

public class OperationInterpreter extends AbstractOperationInterpreter<Operation> {
    @Override
    public Object run(Operation operation, ParseContext parseContext, Context context) {
        final List<Object> values = getValues(operation.getExpressions(), parseContext, context);

        final Class<?> valueClass = getClass(values);

        final Object result;

        switch (operation.getType()) {
            case ADD:
                result = add(values, valueClass);
                break;
            case SUB:
                result = sub(values, valueClass);
                break;
            case MULT:
                result = mult(values, valueClass);
                break;
            case DIV:
                result = div(values, valueClass);
                break;
            default:
                throw new IllegalArgumentException(format("Unsupported operation %s", operation.getType()));
        }

        return result;
    }

    private Object add(List<Object> values, Class<?> tClass) {
        if (tClass.equals(BigDecimal.class)) {
            return calculate(values, BigDecimal.class, BigDecimal::add);
        }

        if (tClass.equals(String.class)) {
            return values
                .stream()
                .map(o -> (String) o)
                .collect(joining());
        }

        throw new IllegalArgumentException(format("Addition for %s is not supported", tClass));
    }

    private Object sub(List<Object> values, Class<?> tClass) {
        if (tClass.equals(BigDecimal.class)) {
            return calculate(values, BigDecimal.class, BigDecimal::subtract);
        }

        throw new IllegalArgumentException(format("Subtraction for %s is not supported", tClass));

    }

    private Object mult(List<Object> values, Class<?> tClass) {
        if (tClass.equals(BigDecimal.class)) {
            return calculate(values, BigDecimal.class, BigDecimal::multiply);
        }

        throw new IllegalArgumentException(format("Multiplication for %s is not supported", tClass));
    }

    private Object div(List<Object> values, Class<?> tClass) {
        if (tClass.equals(BigDecimal.class)) {
            return calculate(values, BigDecimal.class, (v1, v2) -> v1.divide(v2, HALF_UP));
        }

        throw new IllegalArgumentException(format("Division for %s is not supported", tClass));
    }
}
