package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.identifier;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.ObjectProperty;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

public class ObjectPropertyInterpreter extends ExpressionInterpreter<ObjectProperty> {
    public Object run(ObjectProperty objectProperty, ParseContext parseContext, Context context) {
        return context.getVariables().get(objectProperty.getIdentifiers());
    }
}
