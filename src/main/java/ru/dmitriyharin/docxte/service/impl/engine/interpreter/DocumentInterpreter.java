package ru.dmitriyharin.docxte.service.impl.engine.interpreter;

import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.P;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.NumberValue;
import ru.dmitriyharin.docxte.service.engine.interpreter.ObjectsInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction.InstructionInterpreterFactory;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse.DocumentParser;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class DocumentInterpreter implements ObjectsInterpreter {
    public void run(MainDocumentPart documentPart, Context context) {
        final List<Object> objects = unmodifiableList(new ArrayList<>(documentPart.getContent()));

        documentPart.getContent().clear();

        final DocumentParser parser = new DocumentParser();

        final ParseResult parseResult = parser.parse(objects);

        run(parseResult.getTemplate().getItems(), parseResult.getContext(), context)
            .forEach(documentPart::addObject);
    }

    @Override
    public List<Object> run(
        List<LanguageItem> languageItems,
        ParseContext parseContext,
        Context context
    ) {
        final List<Object> resultObjects = new ArrayList<>();

        for (final LanguageItem item : languageItems) {
            if (item instanceof NumberValue) {
                final int index = ((NumberValue) item).getValue().intValue();

                final Object object = parseContext.get(index);

                if (object instanceof P) {
                    final P resultP = processParagraph((P) object, context);

                    if (resultP != null) {
                        resultObjects.add(resultP);
                    }
                } else {
                    resultObjects.add(object);
                }
            } else if (item instanceof Instruction) {
                resultObjects.addAll(processInstruction((Instruction) item, parseContext, context));
            } else {
                throw new IllegalArgumentException("Unexpected language item " + item.getClass());
            }
        }

        return resultObjects;
    }

    private P processParagraph(P p, Context context) {
        final ParagraphInterpreter paragraphInterpreter = new ParagraphInterpreter();

        return paragraphInterpreter.run(p, context);
    }

    private List<Object> processInstruction(
        Instruction instruction,
        ParseContext parseContext,
        Context context
    ) {
        return InstructionInterpreterFactory
            .get(instruction)
            .run(instruction, parseContext, context);
    }
}
