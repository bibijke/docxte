package ru.dmitriyharin.docxte.service.impl.engine.interpreter.context.function.math;

import ru.dmitriyharin.docxte.exception.engine.interpreter.context.ContextException;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.context.function.BaseContextFunction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.lang.String.format;

public class Average extends BaseContextFunction {
    @Override
    public String getName() {
        return "average";
    }

    @Override
    public BigDecimal execute(List<Object> arguments) {
        if (arguments.size() == 0) {
            throw new ContextException(format("Function %s expect 1 or more arguments but 0 given", getName()));
        }

        return arguments
            .stream()
            .map(arg -> (BigDecimal) arg)
            .reduce(BigDecimal.ZERO, BigDecimal::add)
            .divide(BigDecimal.valueOf(arguments.size()), ((BigDecimal) arguments.get(0)).scale(), RoundingMode.HALF_UP);
    }
}
