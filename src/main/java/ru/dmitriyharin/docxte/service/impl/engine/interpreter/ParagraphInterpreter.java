package ru.dmitriyharin.docxte.service.impl.engine.interpreter;

import org.docx4j.XmlUtils;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.RunRange;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.NumberValue;
import ru.dmitriyharin.docxte.service.engine.interpreter.ObjectsInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction.InstructionInterpreterFactory;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse.ParagraphParser;

import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.isEmpty;

public class ParagraphInterpreter implements ObjectsInterpreter {
    public P run(P p, Context context) {
        final ParagraphParser parser = new ParagraphParser();

        final ParseResult parseResult = parser.parse(p);

        final P resultP = XmlUtils.deepCopy(p);
        resultP.getContent().clear();

        final List<Object> resultObjects = run(
            parseResult.getTemplate().getItems(),
            parseResult.getContext(),
            context
        );

        if (isEmpty(resultObjects)) {
            return null;
        }

        resultP
            .getContent()
            .addAll(resultObjects);

        return resultP;
    }

    @Override
    public List<Object> run(
        List<LanguageItem> languageItems,
        ParseContext parseContext,
        Context context
    ) {
        final List<Object> resultObjects = new ArrayList<>();

        for (final LanguageItem item : languageItems) {
            if (item instanceof NumberValue) {
                final int index = ((NumberValue) item).getValue().intValue();

                final Object object = parseContext.get(index);

                if (object instanceof RunRange) {
                    resultObjects.add(processRun((RunRange) object));
                } else {
                    resultObjects.add(object);
                }
            } else if (item instanceof Instruction) {
                resultObjects.addAll(processInstruction((Instruction) item, parseContext, context));
            } else {
                throw new IllegalArgumentException("Unexpected language item " + item.getClass());
            }
        }

        return resultObjects;
    }

    private R processRun(RunRange runRange) {
        final R resultR = XmlUtils.deepCopy(runRange.getR());

        final Object elValue = ((JAXBElement<?>) resultR.getContent().get(0))
            .getValue();

        final Text t = ((Text) elValue);

        t.setValue(runRange.getModifiedText());
        t.setSpace("preserve"); // This line disabled space ignoring in text value

        return resultR;
    }

    private List<Object> processInstruction(
        Instruction instruction,
        ParseContext parseContext,
        Context context
    ) {
        return InstructionInterpreterFactory
            .get(instruction)
            .run(instruction, parseContext, context);
    }
}
