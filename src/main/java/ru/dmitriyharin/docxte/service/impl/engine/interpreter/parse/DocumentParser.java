package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import org.apache.commons.lang3.StringUtils;
import org.docx4j.TextUtils;
import org.docx4j.wml.P;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.grammar.LanguageGrammar;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.Scope;

import java.util.List;

public class DocumentParser extends BaseParser<List<Object>> {
    @Override
    public ParseResult parse(List<Object> objects) {
        final StringBuilder stringBuilder = new StringBuilder();

        final ParseContext parseContext = new ParseContext(Scope.DOCUMENT);

        for (final Object object : objects) {
            boolean isInstruction = false;

            if (object instanceof P) {
                final String pText = TextUtils.getText(object).trim();

                if (isInstruction(pText)) {
                    stringBuilder
                        .append(pText)
                        .append("\r\n");

                    isInstruction = true;
                }
            }

            if (!isInstruction) {
                stringBuilder
                    .append(parseContext.add(object))
                    .append("\r\n");
            }
        }

        return new ParseResult(
            parse(stringBuilder.toString()),
            parseContext
        );
    }

    private boolean isInstruction(String text) {
        return text.startsWith(LanguageGrammar.OPEN_TAG) &&
            text.endsWith(LanguageGrammar.CLOSE_TAG) &&
            StringUtils.countMatches(text, LanguageGrammar.OPEN_TAG) == 1;
    }
}
