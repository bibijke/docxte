package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction;

import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.condition.Condition;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.cycle.Cycle;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.display.Display;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.instruction.InstructionInterpreter;

@SuppressWarnings("unchecked")
public class InstructionInterpreterFactory {
    public static  <T extends Instruction> InstructionInterpreter<T> get(T instruction) {
        if (instruction instanceof Condition) {
            return (InstructionInterpreter<T>) new ConditionInterpreter();
        }

        if (instruction instanceof Cycle) {
            return (InstructionInterpreter<T>) new CycleInterpreter();
        }

        if (instruction instanceof Display) {
            return (InstructionInterpreter<T>) new DisplayInterpreter();
        }

        throw new IllegalArgumentException("Unsupported instruction " + instruction.getClass());
    }
}