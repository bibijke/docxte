package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.value;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.Value;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

public class ValueInterpreter extends ExpressionInterpreter<Value<?>> {
    @Override
    public Object run(Value<?> value, ParseContext parseContext, Context context) {
        return value.getValue();
    }
}
