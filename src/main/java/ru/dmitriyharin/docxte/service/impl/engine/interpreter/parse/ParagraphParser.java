package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import org.docx4j.XmlUtils;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.Scope;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.ReadMode;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.RunRange;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.SearchContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.TagPos;

import javax.xml.bind.JAXBElement;
import java.util.Collection;
import java.util.List;

import static ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.ReadMode.SEARCH_FOR_CLOSE_TAG;
import static ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph.ReadMode.SEARCH_FOR_OPEN_TAG;

public class ParagraphParser extends BaseParser<P> {
    @Override
    public ParseResult parse(P p) {
        final List<Object> objects = p.getContent();

        final StringBuilder stringBuilder = new StringBuilder();

        final ParseContext parseContext = new ParseContext(Scope.PARAGRAPH);

        final SearchContext searchContext = new SearchContext();

        for (final Object object : objects) {
            if (object instanceof R) {
                final R r = XmlUtils.deepCopy((R) object);

                if (r.getContent().size() != 1) {
                    throw new IllegalStateException("Unsupported word document: run contains zero or more than one elements");
                }

                parseR(parseContext, stringBuilder, r, searchContext);
            } else {
                if (searchContext.getReadMode() == SEARCH_FOR_OPEN_TAG) {
                    stringBuilder
                        .append(parseContext.add(object))
                        .append("\r\n");
                }
            }
        }

        return new ParseResult(
            parse(stringBuilder.toString()),
            parseContext
        );
    }

    private void parseR(ParseContext parseContext, StringBuilder stringBuilder, R r, SearchContext context) {
        moveUncompletedTagPartsToRun(r, parseContext, context, stringBuilder);

        String text = getRunText(r);

        int runPos = 0;

        if (text != null) {
            while (text != null && text.length() > 0) {
                if (context.getReadMode() == SEARCH_FOR_OPEN_TAG) {
                    final TagPos tagPos = findNearestTag(text, context.getCurrentPossibleTags());

                    if (tagPos != null) {
                        final int pos = tagPos.getPos();

                        if (pos > 0) {
                            stringBuilder
                                .append(parseContext.add(new RunRange(r, runPos, runPos + pos)))
                                .append("\r\n");
                        }

                        final int endPos = pos + tagPos.getTag().length();

                        runPos += endPos;

                        stringBuilder.append(text, pos, endPos);

                        text = text.substring(endPos);

                        context.setReadMode(ReadMode.SEARCH_FOR_CLOSE_TAG);
                        context.setLastOpenTag(tagPos.getTag());
                    } else {
                        stringBuilder
                            .append(parseContext.add(new RunRange(r, runPos)))
                            .append("\r\n");

                        text = null;
                    }

                    continue;
                }

                if (context.getReadMode() == ReadMode.SEARCH_FOR_CLOSE_TAG) {
                    final TagPos tagPos = findNearestTag(text, context.getCurrentPossibleTags());

                    if (tagPos != null) {
                        final int pos = tagPos.getPos();

                        final int endPos = pos + tagPos.getTag().length();

                        runPos += endPos;

                        stringBuilder
                            .append(text, 0, endPos)
                            .append("\r\n");

                        text = text.substring(endPos);

                        context.setReadMode(SEARCH_FOR_OPEN_TAG);
                        context.setLastOpenTag(null);
                    } else {
                        stringBuilder.append(text);

                        text = null;
                    }
                } else {
                    throw new IllegalStateException("Unsupported read mode " + context.getReadMode());
                }
            }
        } else {
            if (context.getReadMode() == SEARCH_FOR_OPEN_TAG) {
                stringBuilder
                    .append(parseContext.add(r))
                    .append("\r\n");
            }
        }
    }

    private String getRunText(R r) {
        final Object rContent = r.getContent().get(0);

        if (!(rContent instanceof JAXBElement<?>)) {
            return null;
        }

        final Object elValue = ((JAXBElement<?>) rContent).getValue();

        if (!(elValue instanceof Text)) {
            return null;
        }

        return ((Text) elValue).getValue();
    }

    private void setRunText(R r, String text) {
        final Object rContent = r.getContent().get(0);

        if (!(rContent instanceof JAXBElement<?>)) {
            throw new IllegalArgumentException("Only runs with text content are supported");
        }

        final Object elValue = ((JAXBElement<?>) rContent).getValue();

        if (!(elValue instanceof Text)) {
            throw new IllegalArgumentException("Only runs with text content are supported");
        }

        ((Text) elValue).setValue(text);
    }

    // Move tag parts to input run from previous run
    private void moveUncompletedTagPartsToRun(
        R r,
        ParseContext parseContext,
        SearchContext context,
        StringBuilder stringBuilder
    ) {
        String text = getRunText(r);

        if (text == null) {
            return;
        }

        final Collection<String> possibleTags = context.getCurrentPossibleTags();

        for (final String tag : possibleTags) {
            int tagEndPos = findTagEndPos(text, tag);

            if (tagEndPos < 0) {
                tagEndPos = text.length();
            }

            final String sub = text.substring(0, tagEndPos);

            if (!tag.equals(sub) && tag.endsWith(sub)) {
                final String searchFor = tag.substring(0, tag.length() - sub.length());

                if (SEARCH_FOR_OPEN_TAG == context.getReadMode()) {
                    final Object prevObject = parseContext.getLast();

                    if (prevObject instanceof RunRange) {
                        final RunRange prevRange = (RunRange) prevObject;

                        final String prevRangeText = prevRange.getSourceText();

                        if (prevRangeText.endsWith(searchFor)) {
                            if (!prevRangeText.equals(searchFor)) {
                                prevRange.setEndPos(prevRangeText.length() - searchFor.length());
                            } else {
                                parseContext.removeLast();
                            }

                            setRunText(r, searchFor + text);

                            break;
                        }
                    }
                }

                if (SEARCH_FOR_CLOSE_TAG == context.getReadMode()) {
                    final String tagStartPart = stringBuilder
                        .substring(stringBuilder.length() - searchFor.length());

                    if (searchFor.equals(tagStartPart)) {
                        stringBuilder.delete(stringBuilder.length() - searchFor.length(), stringBuilder.length());

                        setRunText(r, searchFor + text);

                        break;
                    }
                }
            }
        }
    }

    private TagPos findNearestTag(String text, Collection<String> possibleTags) {
        return possibleTags
            .stream()
            .map(tag -> new TagPos(tag, text.indexOf(tag)))
            .filter(tagPos -> tagPos.getPos() >= 0)
            .reduce((tagPos1, tagPos2) -> tagPos1.getPos() <= tagPos2.getPos() ? tagPos1 : tagPos2)
            .orElse(null);
    }

    // Find tag end position if it's first N symbols in previous run text
    private int findTagEndPos(String text, String tag) {
        int pos = -1;

        for (int i = 1; i < tag.length(); i++) {
            final String tagEnd = tag.substring(i);

            if (text.startsWith(tagEnd)) {
                return tagEnd.length();
            }
        }

        return pos;
    }
}
