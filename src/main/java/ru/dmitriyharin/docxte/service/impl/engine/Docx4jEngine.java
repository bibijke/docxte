package ru.dmitriyharin.docxte.service.impl.engine;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.exception.engine.EngineException;
import ru.dmitriyharin.docxte.service.engine.TemplateEngine;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.DocumentInterpreter;

import java.io.InputStream;
import java.io.OutputStream;

public class Docx4jEngine implements TemplateEngine {
    @Override
    public void process(InputStream source, OutputStream out, Context context) throws EngineException {
        final WordprocessingMLPackage wordMLPackage;

        try {
            wordMLPackage = WordprocessingMLPackage.load(source);
        } catch (Docx4JException e) {
            throw new EngineException("Unable to load document source", e);
        }

        final DocumentInterpreter interpreter = new DocumentInterpreter();

        interpreter
            .run(wordMLPackage.getMainDocumentPart(), context);

        try {
            wordMLPackage.save(out);
        } catch (Docx4JException e) {
            throw new EngineException("Unable to save modified document", e);
        }
    }
}
