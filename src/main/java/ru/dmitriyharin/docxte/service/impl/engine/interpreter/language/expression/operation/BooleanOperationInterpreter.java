package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.operation;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;

import java.util.List;

import static java.lang.String.format;

public class BooleanOperationInterpreter extends AbstractOperationInterpreter<BooleanOperation> {
    @Override
    public Object run(BooleanOperation operation, ParseContext parseContext, Context context) {
        final List<Object> values = getValues(operation.getExpressions(), parseContext, context);

        final Class<?> valueClass = getClass(values);

        final Object result;

        switch (operation.getType()) {
            case AND:
                result = and(values, valueClass);
                break;
            case OR:
                result = or(values, valueClass);
                break;
            default:
                throw new IllegalArgumentException(format("Unsupported operation %s", operation.getType()));
        }

        return result;
    }

    public Object and(List<Object> values, Class<?> tClass) {
        if (tClass.equals(Boolean.class)) {
            return calculate(values, Boolean.class, Boolean::logicalAnd);
        }

        throw new IllegalArgumentException(format("Logical operation AND for %s is not supported", tClass));
    }

    public Object or(List<Object> values, Class<?> tClass) {
        if (tClass.equals(Boolean.class)) {
            return calculate(values, Boolean.class, Boolean::logicalOr);
        }

        throw new IllegalArgumentException(format("Logical operation AND for %s is not supported", tClass));
    }
}
