package ru.dmitriyharin.docxte.service.impl.engine.interpreter;

import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.Scope;
import ru.dmitriyharin.docxte.service.engine.interpreter.ObjectsInterpreter;

public class ObjectsInterpreterFactory {
    public static ObjectsInterpreter get(Scope scope) {
        switch (scope) {
            case DOCUMENT:
                return new DocumentInterpreter();
            case PARAGRAPH:
                return new ParagraphInterpreter();
        }

        throw new IllegalArgumentException("Unsupported scope " + scope);
    }
}
