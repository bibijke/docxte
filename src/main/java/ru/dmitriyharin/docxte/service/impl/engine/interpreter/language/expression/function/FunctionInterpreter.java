package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.function;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function.Function;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

import java.util.List;

public class FunctionInterpreter extends ExpressionInterpreter<Function> {
    @Override
    public Object run(Function function, ParseContext parseContext, Context context) {
        final List<Object> arguments = evaluate(function.getArguments().getItems(), parseContext, context);

        return context
            .getFunctions()
            .get(function.getName())
            .execute(arguments);
    }
}
