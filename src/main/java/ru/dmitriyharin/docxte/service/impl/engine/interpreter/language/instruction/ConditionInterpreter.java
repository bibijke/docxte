package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction;

import org.apache.commons.lang3.tuple.Pair;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.condition.Condition;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.instruction.InstructionInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.ObjectsInterpreterFactory;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreterFactory;

import java.util.Collections;
import java.util.List;

public class ConditionInterpreter implements InstructionInterpreter<Condition> {
    @Override
    public List<Object> run(Condition condition, ParseContext parseContext, Context context) {
        for (final Pair<BooleanExpression, List<LanguageItem>> rule : condition.getRules()) {
            final BooleanExpression expression = rule.getKey();

            final Boolean isTrue = ExpressionInterpreterFactory
                .get(expression)
                .run(expression, Boolean.class, parseContext, context);

            if (isTrue) {
                return ObjectsInterpreterFactory
                    .get(parseContext.getScope())
                    .run(rule.getRight(), parseContext, context);
            }
        }

        return Collections.emptyList();
    }
}