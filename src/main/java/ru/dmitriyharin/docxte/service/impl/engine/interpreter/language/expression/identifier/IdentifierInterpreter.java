package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.identifier;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

public class IdentifierInterpreter extends ExpressionInterpreter<Identifier> {
    @Override
    public Object run(Identifier identifier, ParseContext parseContext, Context context) {
        return context.getVariables().get(identifier);
    }
}
