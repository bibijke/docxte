package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression;

import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison.Comparison;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.Operation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function.Function;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.ObjectProperty;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.Value;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.comparison.ComparisonInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.operation.BooleanOperationInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.operation.OperationInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.function.FunctionInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.identifier.IdentifierInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.identifier.ObjectPropertyInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.value.ValueInterpreter;

@SuppressWarnings("unchecked")
public class ExpressionInterpreterFactory {
    public static <T extends Expression> ExpressionInterpreter<T> get(T expression) {
        if (expression instanceof Value) {
            return (ExpressionInterpreter<T>) new ValueInterpreter();
        }

        if (expression instanceof Identifier) {
            return (ExpressionInterpreter<T>) new IdentifierInterpreter();
        }

        if (expression instanceof ObjectProperty) {
            return (ExpressionInterpreter<T>) new ObjectPropertyInterpreter();
        }

        if (expression instanceof Function) {
            return (ExpressionInterpreter<T>) new FunctionInterpreter();
        }

        if (expression instanceof Operation) {
            return (ExpressionInterpreter<T>) new OperationInterpreter();
        }

        if (expression instanceof BooleanOperation) {
            return (ExpressionInterpreter<T>) new BooleanOperationInterpreter();
        }

        if (expression instanceof Comparison) {
            return (ExpressionInterpreter<T>) new ComparisonInterpreter();
        }

        throw new IllegalArgumentException("Unsupported expression " + expression.getClass());

    }
}
