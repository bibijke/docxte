package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.comparison;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison.Comparison;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison.ComparisonType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

import java.util.List;

import static java.lang.String.format;

public class ComparisonInterpreter extends ExpressionInterpreter<Comparison> {
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public Boolean run(Comparison comparison, ParseContext parseContext, Context context) {
        final List<Object> values = evaluate(comparison.getExpressions(), parseContext, context);

        if (values.size() != 2) {
            throw new IllegalArgumentException("Comparison expects both sides left and right");
        }

        final Object left = values.get(0);

        final Object right = values.get(1);

        if (!left.getClass().equals(right.getClass())) {
            throw new IllegalArgumentException("Unable to compare values with different types");
        }

        if (left instanceof Comparable) {
            return compare((Comparable) left, (Comparable) right, comparison.getType());
        }

        throw new IllegalArgumentException(format("Unable to compare %s", left.getClass()));
    }

    private <T extends Comparable<T>> boolean compare(T left, T right, ComparisonType type) {
        switch (type) {
            case EQ:
                return left.equals(right);
            case NEQ:
                return !left.equals(right);
            case GT:
                return left.compareTo(right) > 0;
            case GTE:
                return left.compareTo(right) >= 0;
            case LT:
                return left.compareTo(right) < 0;
            case LTE:
                return left.compareTo(right) <= 0;
        }

        throw new IllegalArgumentException("Unsupported comparison type " + type);
    }
}
