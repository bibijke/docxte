package ru.dmitriyharin.docxte.service.impl.engine.antlr.visitor;

import lombok.RequiredArgsConstructor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import ru.ahml.apxd.antlr.DocxTEBaseVisitor;
import ru.ahml.apxd.antlr.DocxTEParser;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison.Comparison;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison.ComparisonType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperationType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.OperationType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.Operation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function.Arguments;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.ObjectProperty;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function.Function;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.StringValue;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.cycle.Cycle;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.display.Display;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.BooleanValue;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.EOF;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.Template;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.condition.Condition;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.NumberValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
public class TemplateVisitor extends DocxTEBaseVisitor<LanguageItem> {
    @Override
    public Template visitTemplate(DocxTEParser.TemplateContext ctx) {
        final List<LanguageItem> items = new ArrayList<>();

        for (final ParseTree child : ctx.children) {
            final LanguageItem item = visit(child);

            Validate.notNull(item, "Unable to process node " + child);

            if (!(item instanceof EOF)) {
                items.add(item);
            }
        }

        return new Template(items);
    }

    @Override
    public LanguageItem visitInstruction(DocxTEParser.InstructionContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public Condition visitCondition(DocxTEParser.ConditionContext ctx) {
        if (ctx.children.size() < 1) {
            throw new IllegalArgumentException("Unable to parse condition");
        }

        final List<Pair<BooleanExpression, List<LanguageItem>>> rules = new ArrayList<>();

        Pair<BooleanExpression, List<LanguageItem>> rule = null;

        for (final ParseTree child : ctx.children) {
            final LanguageItem item = visit(child);

            if (item instanceof BooleanExpression) {
                if (rule != null) {
                    rules.add(rule);
                }

                rule = new ImmutablePair<>((BooleanExpression) item, new ArrayList<>());
            } else {
                if (rule == null || rule.getRight() == null) {
                    throw new IllegalStateException("Previous rule can't be null");
                }

                rule
                    .getRight()
                    .add(item);
            }
        }

        return new Condition(rules);
    }

    @Override
    public BooleanExpression visitIf_(DocxTEParser.If_Context ctx) {
        return (BooleanExpression) visit(ctx.expr());
    }

    @Override
    public BooleanExpression visitElseif_(DocxTEParser.Elseif_Context ctx) {
        return (BooleanExpression) visit(ctx.expr());
    }

    @Override
    public BooleanExpression visitElse_(DocxTEParser.Else_Context ctx) {
        return new BooleanValue(true);
    }

    @Override
    public BooleanExpression visitEndif_(DocxTEParser.Endif_Context ctx) {
        return new BooleanValue(true);
    }

    @Override
    public Cycle visitCycle(DocxTEParser.CycleContext ctx) {
        final List<LanguageItem> items = new ArrayList<>();

        for (int i = 1; i < ctx.children.size() - 1; i++) {
            items.add(visit(ctx.children.get(i)));
        }

        final Identifier key = (Identifier) visit(ctx.for_().forKey());

        final Expression source = (Expression) visit(ctx.for_().forSource());

        final Identifier loopKey = Optional
            .ofNullable(ctx.for_().forLoop())
            .map(loop -> (Identifier) visit(loop))
            .orElse(null);

        return new Cycle(key, source, loopKey, items);
    }

    @Override
    public Identifier visitForKey(DocxTEParser.ForKeyContext ctx) {
        return (Identifier) visit(ctx.ID());
    }

    @Override
    public Expression visitForSource(DocxTEParser.ForSourceContext ctx) {
        return ctx.function() != null ?
            (Function) visit(ctx.function()) :
            (Identifier) visit(ctx.ID());
    }

    @Override
    public Identifier visitForLoop(DocxTEParser.ForLoopContext ctx) {
        return (Identifier) visit(ctx.ID());
    }

    @Override
    public Display visitDisplay(DocxTEParser.DisplayContext ctx) {
        return new Display((Expression) visit(ctx.expr()));
    }

    @Override
    public Function visitFunc(DocxTEParser.FuncContext ctx) {
        return (Function) visitChildren(ctx);
    }

    @Override
    public Function visitFunction(DocxTEParser.FunctionContext ctx) {
        return new Function(
            (Identifier) visit(ctx.ID()),
            (Arguments) visit(ctx.arguments())
        );
    }

    @Override
    public Arguments visitArguments(DocxTEParser.ArgumentsContext ctx) {
        return new Arguments(visitExpressions(ctx.expr()));
    }

    @Override
    public Expression visitParExpr(DocxTEParser.ParExprContext ctx) {
        return (Expression) visit(ctx.expr());
    }

    @Override
    public ObjectProperty visitObjectProperty(DocxTEParser.ObjectPropertyContext ctx) {
        final List<Identifier> identifiers = ctx
            .ID()
            .stream()
            .map(idNode -> (Identifier) visit(idNode))
            .collect(toList());

        return new ObjectProperty(identifiers);
    }

    @Override
    public Operation visitMultiplicationExpr(DocxTEParser.MultiplicationExprContext ctx) {
        final OperationType operation = (ctx.op.getType() == DocxTEParser.DIV) ?
            OperationType.DIV :
            OperationType.MULT;

        return new Operation(operation, visitExpressions(ctx.expr()));
    }

    @Override
    public LanguageItem visitAdditiveExpr(DocxTEParser.AdditiveExprContext ctx) {
        final OperationType operation = (ctx.op.getType() == DocxTEParser.MINUS) ?
            OperationType.SUB :
            OperationType.ADD;

        return new Operation(operation, visitExpressions(ctx.expr()));
    }

    @Override
    public Comparison visitRelationalExpr(DocxTEParser.RelationalExprContext ctx) {
        final ComparisonType type;

        switch (ctx.op.getType()) {
            case DocxTEParser.GT:
                type = ComparisonType.GT;
                break;
            case DocxTEParser.GTEQ:
                type = ComparisonType.GTE;
                break;
            case DocxTEParser.LT:
                type = ComparisonType.LT;
                break;
            case DocxTEParser.LTEQ:
                type = ComparisonType.LTE;
                break;
            default:
                throw new IllegalArgumentException("Unsupported comparison type " + ctx.op.getText());
        }

        return new Comparison(type, visitExpressions(ctx.expr()));
    }

    @Override
    public Comparison visitEqualityExpr(DocxTEParser.EqualityExprContext ctx) {
        final ComparisonType type = (ctx.op.getType() == DocxTEParser.NEQ) ?
            ComparisonType.NEQ :
            ComparisonType.EQ;

        return new Comparison(type, visitExpressions(ctx.expr()));
    }

    @Override
    public Identifier visitIdAtom(DocxTEParser.IdAtomContext ctx) {
        return new Identifier(ctx.ID().getText());
    }

    @Override
    public BooleanValue visitBool(DocxTEParser.BoolContext ctx) {
        return new BooleanValue(Boolean.valueOf(ctx.getText().toLowerCase()));
    }

    @Override
    public BooleanOperation visitAndExpr(DocxTEParser.AndExprContext ctx) {
        return new BooleanOperation(BooleanOperationType.AND, visitExpressions(ctx.expr(), BooleanExpression.class));
    }

    @Override
    public Expression visitOrExpr(DocxTEParser.OrExprContext ctx) {
        return new BooleanOperation(BooleanOperationType.OR, visitExpressions(ctx.expr(), BooleanExpression.class));
    }

    @Override
    public LanguageItem visitAtomExpr(DocxTEParser.AtomExprContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public BooleanValue visitBooleanAtom(DocxTEParser.BooleanAtomContext ctx) {
        return ctx.TRUE() != null ?
            new BooleanValue(true) :
            new BooleanValue(false);
    }

    @Override
    public LanguageItem visitStringAtom(DocxTEParser.StringAtomContext ctx) {
        return new StringValue(StringUtils.strip(ctx.STRING().getText(), "\""));
    }

    @Override
    public LanguageItem visitNumberAtom(DocxTEParser.NumberAtomContext ctx) {
        return new NumberValue(new BigDecimal(ctx.NUMBER().getText()));
    }

    @Override
    public LanguageItem visitTerminal(TerminalNode node) {
        switch (node.getSymbol().getType()) {
            case DocxTEParser.NUMBER:
                return new NumberValue(new BigDecimal(node.getText()));
            case DocxTEParser.ID:
                return new Identifier(node.getText());
            case DocxTEParser.EOF:
                return new EOF(null);
        }

        throw new IllegalArgumentException("Unsupported node " + node);
    }

    private List<Expression> visitExpressions(List<DocxTEParser.ExprContext> exprContexts) {
        return visitExpressions(exprContexts, Expression.class);
    }

    private <T extends Expression> List<T> visitExpressions(
        List<DocxTEParser.ExprContext> exprContexts,
        Class<T> expressionClass
    ) {
        return exprContexts
            .stream()
            .map(expr -> expressionClass.cast(visit(expr)))
            .collect(toList());
    }
}
