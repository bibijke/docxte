package ru.dmitriyharin.docxte.service.impl.engine.interpreter.context.value;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.compress.utils.Sets;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ValueConverter {
    private final ObjectMapper mapper = new ObjectMapper();

    private final Set<Class<?>> allowedClasses = Sets.newHashSet(
        Boolean.class,
        String.class,
        BigDecimal.class,
        LocalDateTime.class
    );

    public Object convert(Object value) {
        if (value == null) {
            return null;
        }

        if (allowedClasses.contains(value.getClass())) {
            return value;
        }

        if (value instanceof Number) {
            return convertNumber((Number) value);
        }

        if (value instanceof Map) {
            return convertMap((Map<?, ?>) value);
        }

        if (value instanceof Collection) {
            return convertCollection((Collection<?>) value);
        }

        return convertObject(value);
    }

    private BigDecimal convertNumber(Number number) {
        return BigDecimal.valueOf(number.doubleValue());
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> convertMap(Map<?, ?> map) {
        try {
            return convertMapStringObject((Map<String, Object>) map);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Only Map<String, Object> is supported", e);
        }
    }

    private Map<String, Object> convertMapStringObject(Map<String, Object> map) {
        final Map<String, Object> resultMap = new HashMap<>();

        map.forEach((key, value) -> resultMap.put(key, convert(value)));

        return resultMap;
    }

    private Collection<?> convertCollection(Collection<?> collection) {
        Collector<Object, ?, ? extends Collection<?>> collector = null;

        if (collection instanceof List) {
            collector = Collectors.toList();
        }

        if (collection instanceof Set) {
            collector = Collectors.toSet();
        }

        if (collector == null) {
            throw new IllegalArgumentException("Unsupported collection " + collection.getClass());
        }

        return collection
            .stream()
            .map(this::convert)
            .collect(collector);
    }

    private Map<String, Object> convertObject(Object value) {
        try {
            final String json = mapper.writeValueAsString(value);

            return convertMap(mapper.readValue(json, Map.class));
        } catch (IOException e) {
            throw new IllegalArgumentException("Unsupported value type " + value.getClass(), e);
        }
    }
}
