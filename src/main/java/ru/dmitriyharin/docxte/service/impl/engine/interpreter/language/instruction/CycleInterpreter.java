package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.cycle.LoopContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.cycle.Cycle;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.instruction.InstructionInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.ObjectsInterpreterFactory;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreterFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CycleInterpreter implements InstructionInterpreter<Cycle> {
    @Override
    public List<Object> run(Cycle cycle, ParseContext parseContext, Context context) {
        final List<Object> result = new ArrayList<>();

        final Expression expression = cycle.getSource();

        final Collection<?> sourceCollection = ExpressionInterpreterFactory
            .get(expression)
            .run(expression, Collection.class, parseContext, context);

        int index = 1;

        for (final Object item : sourceCollection) {
            final Identifier loopKey = cycle.getLoopKey();

            context.getVariables().set(cycle.getKey(), item);

            if (loopKey != null) {
                context.getVariables().set(loopKey, createLoopContext(index, sourceCollection.size()));
            }

            result.addAll(run(cycle.getBodyItems(), parseContext, context));

           context.getVariables().unset(cycle.getKey());

            if (loopKey != null) {
                context.getVariables().unset(loopKey);
            }

            index++;
        }

        return result;
    }

    private List<Object> run(List<LanguageItem> items, ParseContext parseContext, Context context) {
        return ObjectsInterpreterFactory
            .get(parseContext.getScope())
            .run(items, parseContext, context);
    }

    private LoopContext createLoopContext(int index, int size) {
        return new LoopContext(
            BigDecimal.valueOf(index),
            index == 1,
            index == size,
            BigDecimal.valueOf(size)
        );
    }
}
