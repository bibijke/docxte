package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.operation;

import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreter;

import java.util.List;
import java.util.Objects;
import java.util.function.BinaryOperator;

public abstract class AbstractOperationInterpreter<T extends Expression> extends ExpressionInterpreter<T> {
    protected List<Object> getValues(List<? extends Expression> expressions, ParseContext parseContext, Context context) {
        final List<Object> values = evaluate(expressions, parseContext, context);

        final boolean hasNullValues = values
            .stream()
            .anyMatch(Objects::isNull);

        if (hasNullValues) {
            throw new IllegalArgumentException("All operations do not support null values");
        }

        final Class<?> valueClass = values.get(0).getClass();

        final boolean hasAnotherTypes = values
            .stream()
            .anyMatch(val -> !val.getClass().equals(valueClass));

        if (hasAnotherTypes) {
            throw new IllegalArgumentException("All operation sides must have same types");
        }

        return values;
    }

    protected Class<?> getClass(List<Object> values) {
        return values.get(0).getClass();
    }

    protected <R> R calculate(List<Object> values, Class<R> tClass, BinaryOperator<R> accumulator) {
        return values
            .stream()
            .map(tClass::cast)
            .reduce(accumulator)
            .orElse(tClass.cast(values.get(0)));
    }
}
