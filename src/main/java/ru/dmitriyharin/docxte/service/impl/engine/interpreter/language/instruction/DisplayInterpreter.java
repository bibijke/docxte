package ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.instruction;

import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.display.Display;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.service.engine.interpreter.language.instruction.InstructionInterpreter;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.language.expression.ExpressionInterpreterFactory;

import java.util.List;

import static java.util.Collections.singletonList;

public class DisplayInterpreter implements InstructionInterpreter<Display> {
    @Override
    public List<Object> run(Display display, ParseContext parseContext, Context context) {
        final Expression expression = display.getExpression();

        final Object value = ExpressionInterpreterFactory
            .get(expression)
            .run(expression, Object.class, parseContext, context);

        return singletonList(createRun(value.toString()));
    }

    private R createRun(String text) {
        final ObjectFactory factory = org.docx4j.jaxb.Context.getWmlObjectFactory();

        final Text t = factory.createText();
        t.setValue(text);

        final R r = factory.createR();
        r
            .getContent()
            .add(t);

        return r;
    }
}
