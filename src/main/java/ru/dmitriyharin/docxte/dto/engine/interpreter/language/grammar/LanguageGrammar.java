package ru.dmitriyharin.docxte.dto.engine.interpreter.language.grammar;

public final class LanguageGrammar {
    private LanguageGrammar() {}

    public final static String OPEN_TAG = "{%";
    public final static String CLOSE_TAG = "%}";

    public final static String DISPLAY_OPEN_TAG = "{{";
    public final static String DISPLAY_CLOSE_TAG = "}}";
}
