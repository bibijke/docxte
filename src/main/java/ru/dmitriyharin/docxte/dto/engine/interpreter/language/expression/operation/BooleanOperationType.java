package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation;

public enum BooleanOperationType {
    AND,
    OR
}
