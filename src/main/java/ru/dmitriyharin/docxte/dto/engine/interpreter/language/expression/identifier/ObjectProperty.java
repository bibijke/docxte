package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

import java.util.List;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class ObjectProperty implements Expression {
    private final List<Identifier> identifiers;
}
