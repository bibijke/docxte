package ru.dmitriyharin.docxte.dto.engine.interpreter.context;

import lombok.Getter;

public class Context {
    @Getter
    private final Variables variables = new Variables();

    @Getter
    private final Functions functions = new Functions();
}
