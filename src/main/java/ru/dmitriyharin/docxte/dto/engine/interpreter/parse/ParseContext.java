package ru.dmitriyharin.docxte.dto.engine.interpreter.parse;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Getter
public final class ParseContext {
    private final Scope scope;

    private final List<Object> objects = new ArrayList<>();

    public int add(Object object) {
        objects.add(object);

        return objects.size() - 1;
    }

    public Object get(int index) {
        return objects.get(index);
    }

    public Object getLast() {
        return objects.size() > 0 ? objects.get(objects.size()-1) : null;
    }

    public void removeLast() {
        objects.remove(objects.get(objects.size()-1));
    }
}
