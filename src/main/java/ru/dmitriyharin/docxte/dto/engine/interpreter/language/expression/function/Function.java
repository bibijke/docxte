package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

@Getter
@RequiredArgsConstructor
public class Function implements Expression {
    private final Identifier name;

    private final Arguments arguments;
}
