package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value;

public final class EOF extends Value<Void> {
    public EOF(Void value) {
        super(value);
    }
}
