package ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.condition;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;

import java.util.List;

@RequiredArgsConstructor
public class Condition implements Instruction {
    @Getter
    private final List<Pair<BooleanExpression, List<LanguageItem>>> rules;
}
