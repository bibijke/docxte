package ru.dmitriyharin.docxte.dto.engine.interpreter.context.cycle;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class LoopContext {
    public BigDecimal index;

    public boolean first;

    public boolean last;

    public BigDecimal count;
}
