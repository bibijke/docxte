package ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TagPos {
    private final String tag;

    private final int pos;
}
