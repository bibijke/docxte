package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison;

public enum ComparisonType {
    EQ,
    NEQ,
    GT,
    GTE,
    LT,
    LTE
}