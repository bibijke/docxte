package ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.cycle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class Cycle implements Instruction {
    private final Identifier key;

    private final Expression source;

    private final Identifier loopKey;

    private final List<LanguageItem> bodyItems;
}
