package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class BooleanOperation implements BooleanExpression {
    private final BooleanOperationType type;

    private final List<BooleanExpression> expressions;
}
