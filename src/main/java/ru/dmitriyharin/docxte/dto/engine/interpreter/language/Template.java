package ru.dmitriyharin.docxte.dto.engine.interpreter.language;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class Template implements LanguageItem {
    @Getter
    private final List<LanguageItem> items;
}
