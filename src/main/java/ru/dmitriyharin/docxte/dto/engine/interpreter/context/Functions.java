package ru.dmitriyharin.docxte.dto.engine.interpreter.context;

import org.apache.commons.lang3.Validate;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.exception.engine.interpreter.context.ContextException;
import ru.dmitriyharin.docxte.service.engine.interpreter.context.ContextFunction;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.context.function.math.Average;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

public final class Functions {
    private final Map<String, ContextFunction> nameFunctionMap = new HashMap<>();

    public Functions() {
        register(new Average());
    }

    public void register(ContextFunction functionImpl) {
        final String name = functionImpl.getName().toLowerCase();

        if (nameFunctionMap.containsKey(name)) {
            throw new ContextException(format("Function %s has been registered already", name));
        }

        nameFunctionMap.put(name, functionImpl);
    }

    public ContextFunction get(Identifier identifier) {
        Validate.notNull(identifier, "Function identifier can't be empty");

        final ContextFunction functionImpl = nameFunctionMap.get(identifier.getName().toLowerCase());

        if (functionImpl == null) {
            throw new ContextException(format("Unknown function %s", identifier.getName()));
        }

        return functionImpl;
    }
}
