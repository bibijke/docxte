package ru.dmitriyharin.docxte.dto.engine.interpreter.parse;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.Template;

@RequiredArgsConstructor
@Data
public class ParseResult {
    private final Template template;

    private final ParseContext context;
}
