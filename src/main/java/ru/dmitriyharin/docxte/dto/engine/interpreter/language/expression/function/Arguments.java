package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.function;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class Arguments implements LanguageItem {
    private final List<Expression> items;
}
