package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.comparison;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class Comparison implements BooleanExpression {
    private final ComparisonType type;

    private final List<Expression> expressions;
}
