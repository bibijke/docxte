package ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph;

public enum ReadMode {
    SEARCH_FOR_OPEN_TAG,
    SEARCH_FOR_CLOSE_TAG
}
