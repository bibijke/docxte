package ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.display;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.Instruction;

@Getter
@RequiredArgsConstructor
public class Display implements Instruction {
    private final Expression expression;
}
