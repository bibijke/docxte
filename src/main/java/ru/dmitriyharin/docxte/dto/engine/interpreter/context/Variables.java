package ru.dmitriyharin.docxte.dto.engine.interpreter.context;

import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.exception.engine.interpreter.context.ContextException;
import ru.dmitriyharin.docxte.service.impl.engine.interpreter.context.value.ValueConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.isEmpty;

public final class Variables {
    private final ValueConverter valueConverter = new ValueConverter();

    private final Map<String, Object> keyValueMap = new HashMap<>();

    public Variables set(Identifier identifier, Object value) {
        return set(singletonList(identifier), value);
    }

    @SuppressWarnings("unchecked")
    public Variables set(List<Identifier> identifiers, Object value) {
        final Object parent = getParent(identifiers);

        final String key = identifiers.get(identifiers.size() - 1).getName();

        if (parent instanceof Map<?, ?>) {
            ((Map<String, Object>) parent).put(key, valueConverter.convert(value));
        } else {
            throw new ContextException(format("Unable to set context value %s", getPath(identifiers)));
        }

        return this;
    }

    public Object get(Identifier identifier) {
        return get(singletonList(identifier));
    }

    public Object get(List<Identifier> identifiers) {
        if (isEmpty(identifiers)) {
            throw new IllegalArgumentException("Identifiers can't be empty");
        }

        Object value = keyValueMap;

        for (int i = 0; i < identifiers.size(); i++) {
            final Identifier identifier = identifiers.get(i);

            final String key = identifier.getName();

            if (value instanceof Map<?, ?>) {
                if (!(((Map<?, ?>) value).containsKey(key))) {
                    throw new ContextException(format("Unknown variable \"%s\"", key));
                }

                value = ((Map<?, ?>) value).get(key);
            } else {
                throw new ContextException(format("Unable to get context value %s", getPath(identifiers)));
            }
        }

        return value;
    }

    @SuppressWarnings("unchecked")
    public void unset(List<Identifier> identifiers) {
        final Object parent = getParent(identifiers);

        final String key = identifiers.get(identifiers.size() - 1).getName();

        if (parent instanceof Map<?, ?>) {
            ((Map<String, Object>) parent).remove(key);
        } else {
            throw new ContextException(format("Unsupported operation on %s", getPath(identifiers)));
        }
    }

    public void unset(Identifier identifier) {
        unset(singletonList(identifier));
    }

    public Object getParent(List<Identifier> identifiers) {
        if (isEmpty(identifiers)) {
            throw new IllegalArgumentException("Identifiers can't be empty");
        }

        return identifiers.size() > 1 ?
            get(identifiers.subList(0, identifiers.size() - 1)) :
            keyValueMap;
    }

    private String getPath(List<Identifier> identifiers) {
        return identifiers
            .stream()
            .map(Identifier::getName)
            .collect(Collectors.joining("."));
    }
}
