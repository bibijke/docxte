package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class Operation implements Expression {
    private final OperationType type;

    private final List<Expression> expressions;
}
