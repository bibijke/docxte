package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value;

import java.math.BigDecimal;

public class NumberValue extends Value<BigDecimal> {
    public NumberValue(BigDecimal value) {
        super(value);
    }
}
