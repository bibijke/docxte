package ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph;

import lombok.*;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;

import javax.xml.bind.JAXBElement;

@Getter
@ToString
public class RunRange {
    private final R r;

    private final int startPos;

    @Setter
    private int endPos;

    private final String sourceText;

    public RunRange(R r, int startPos) {
        this(r, startPos, -1);
    }

    public RunRange(R r, int startPos, int endPos) {
        this.r = r;
        this.startPos = startPos;
        this.endPos = endPos;

        final Object elValue = ((JAXBElement<?>) r.getContent().get(0))
            .getValue();

        this.sourceText = ((Text) elValue).getValue();
    }

    public String getModifiedText() {
        return endPos >= 0 ?
            sourceText.substring(startPos, endPos) :
            sourceText.substring(startPos);
    }
}
