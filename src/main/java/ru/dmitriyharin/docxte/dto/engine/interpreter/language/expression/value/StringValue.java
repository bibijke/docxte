package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value;

public class StringValue extends Value<String> {
    public StringValue(String value) {
        super(value);
    }
}
