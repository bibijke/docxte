package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

@RequiredArgsConstructor
public abstract class Value<T> implements Expression {
    @Getter
    private final T value;
}
