package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation;

public enum OperationType {
    ADD,
    SUB,
    MULT,
    DIV
}
