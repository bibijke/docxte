package ru.dmitriyharin.docxte.dto.engine.interpreter.parse;

public enum Scope {
    DOCUMENT,
    PARAGRAPH
}
