package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;

@RequiredArgsConstructor
@EqualsAndHashCode
public class Identifier implements Expression {
    @Getter
    private final String name;
}