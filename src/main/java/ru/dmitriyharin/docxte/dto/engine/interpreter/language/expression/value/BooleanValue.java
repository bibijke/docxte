package ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value;

import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.BooleanExpression;

public class BooleanValue extends Value<Boolean> implements BooleanExpression {
    public BooleanValue(Boolean value) {
        super(value);
    }
}
