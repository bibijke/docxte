package ru.dmitriyharin.docxte.dto.engine.interpreter.parse.paragraph;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static ru.dmitriyharin.docxte.dto.engine.interpreter.language.grammar.LanguageGrammar.*;
import static ru.dmitriyharin.docxte.dto.engine.interpreter.language.grammar.LanguageGrammar.DISPLAY_CLOSE_TAG;

public class SearchContext {
    @Getter @Setter
    private ReadMode readMode;

    @Getter @Setter
    private String lastOpenTag;

    private final Map<String, String> tagsMap;

    public SearchContext() {
        this.readMode = ReadMode.SEARCH_FOR_OPEN_TAG;
        this.lastOpenTag = null;

        tagsMap = new HashMap<>();

        tagsMap.put(OPEN_TAG, CLOSE_TAG);
        tagsMap.put(DISPLAY_OPEN_TAG, DISPLAY_CLOSE_TAG);
    }

    public Collection<String> getCurrentPossibleTags() {
        if (readMode == ReadMode.SEARCH_FOR_OPEN_TAG) {
            return tagsMap.keySet();
        }

        if (readMode == ReadMode.SEARCH_FOR_CLOSE_TAG) {
            Validate.notNull(lastOpenTag);

            return Collections.singletonList(Validate.notNull(tagsMap.get(lastOpenTag)));
        }

        throw new IllegalStateException("Unsupported read mode " + readMode);
    }
}
