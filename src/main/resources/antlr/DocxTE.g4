// Define a grammar called Hello
grammar DocxTE;

template
:
    (NUMBER|instruction)*? EOF
;

instruction
:
    condition
    | cycle
    | display
;

condition:
    if_ (NUMBER|instruction)*? (elseif_ (NUMBER|instruction)*?)*? (else_ (NUMBER|instruction)*?)*? endif_
;

cycle:
    for_ (NUMBER|instruction)*? endfor_
;

if_: I_START IF expr I_END;
elseif_: I_START ELSEIF expr I_END;
else_: I_START ELSE I_END;
endif_: I_START ENDIF I_END;

for_: I_START FOR forKey IN forSource forLoop? I_END;
forKey: ID;
forSource: (ID|function);
forLoop: LOOP ID;
endfor_:  I_START ENDFOR I_END;

display: D_START expr D_END;

expr
:
    function # Func
    | expr op=(MULT | DIV ) expr  #multiplicationExpr
    | expr op=(PLUS | MINUS) expr  #additiveExpr
    | expr op=(LTEQ | GTEQ | LT | GT) expr #relationalExpr
    | expr op=(EQ | NEQ) expr #equalityExpr
    | expr AND expr #andExpr
    | expr OR expr #orExpr
    | OPAR expr CPAR #parExpr
    | (ID ID_DELIMETER)+ID #objectProperty
    | atom #atomExpr
;

function: ID OPAR arguments? CPAR;

arguments: (expr','?)*;

atom
 : NUMBER  #numberAtom
 | (TRUE | FALSE) #booleanAtom
 | ID             #idAtom
 | STRING         #stringAtom
 ;

bool: TRUE | FALSE;

I_START: '{%';
I_END: '%}';
D_START: '{{';
D_END: '}}';

OR: 'or';
AND: 'and';
GT: '>';
LT: '<';
GTEQ: '>=';
LTEQ: '<=';
EQ: '==';
NEQ: '!=';
PLUS: '+';
MINUS: '-';
MULT: '*';
DIV: '/';

SCOL: ';';
ASSIGN: ':=';
OPAR: '(';
CPAR: ')';
TRUE: 'true';
FALSE: 'false';
IF: 'if';
ELSEIF: 'elseif';
ELSE: 'else';
ENDIF: 'endif';
FOR: 'for';
ENDFOR: 'endfor';
IN: 'in';
LOOP: 'loop';
ID_DELIMETER: '.';

NUMBER: ( [0-9]* '.' )? [0-9]+;
ID: [a-zA-Z_] [a-zA-Z0-9_]*;
SPACE: [ \t\r\n]+ -> skip;
STRING: '"' (~["\r\n] | '""')* '"';