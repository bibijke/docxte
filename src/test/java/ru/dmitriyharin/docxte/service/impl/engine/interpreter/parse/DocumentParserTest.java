package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.junit.jupiter.api.Test;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.LanguageItem;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.condition.Condition;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static ru.dmitriyharin.docxte.dto.engine.interpreter.parse.Scope.DOCUMENT;

public class DocumentParserTest {
    private final DocumentParser parser = new DocumentParser();

    @Test
    public void testParse() throws Exception {
        final MainDocumentPart part = new MainDocumentPart();

        final List<Object> objects = asList(
            part.createParagraphOfText("{% if false %}"),
            new Object(),
            part.createParagraphOfText("Text 1"),
            part.createParagraphOfText("{% endif %}")
        );

        final ParseResult result = parser.parse(objects);

        final List<LanguageItem> items = result.getTemplate().getItems();

        final ParseContext context = result.getContext();

        assertEquals(1, items.size());

        assertTrue(items.get(0) instanceof Condition);

        assertEquals(DOCUMENT, context.getScope());
        assertEquals(2, context.getObjects().size());

        assertSame(objects.get(1), context.get(0));
        assertSame(objects.get(2), context.get(1));
    }
}
