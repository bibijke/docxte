package ru.dmitriyharin.docxte.service.impl;

import org.docx4j.TextUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.junit.jupiter.api.Test;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;
import ru.dmitriyharin.docxte.exception.engine.EngineException;
import ru.dmitriyharin.docxte.service.engine.TemplateEngine;
import ru.dmitriyharin.docxte.service.impl.engine.Docx4jEngine;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Docx4jEngineTest {
    @Test
    public void testProcess() throws Exception {
        final TemplateEngine engine = new Docx4jEngine();

        final Context context = new Context();

        context
            .getVariables()
            .set(new Identifier("keys"), asList(1L, 2L));

        final byte[] outBytes;

        try (
            final InputStream source = getClass().getClassLoader().getResourceAsStream("docs/docx4jEngine.docx");
            final ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            engine.process(source, out, context);

            outBytes = out.toByteArray();
        }

        assertTrue(outBytes.length > 0);

        final WordprocessingMLPackage wordMLPackage;

        try {
            wordMLPackage = WordprocessingMLPackage.load(new ByteArrayInputStream(outBytes));
        } catch (Docx4JException e) {
            throw new EngineException("Unable to load document source", e);
        }

        final MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();

        final List<Object> objects = documentPart.getContent();

        assertEquals(3, objects.size());

        assertEquals("Цикл 1.0", TextUtils.getText(objects.get(0)));
        assertEquals("Цикл 2.0", TextUtils.getText(objects.get(1)));
        assertEquals("1 + 2 = 3", TextUtils.getText(objects.get(2)));
    }
}
