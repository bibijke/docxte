package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import org.junit.jupiter.api.Test;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.Expression;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.BooleanOperationType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.Operation;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.operation.OperationType;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.value.Value;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.instruction.display.Display;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;


public class BaseParserTest {
    final BaseParser<List<String>> parser = new BaseParserStub();

    @Test
    public void testParse_Values() {
        testValue("true", true);
        testValue("false", false);
        testValue("0", BigDecimal.ZERO);
        testValue("1.25", BigDecimal.valueOf(1.25));
        testValue("\"string\"", "string");
    }

    @Test
    public void testParse_Operations() {
        testOperation(OperationType.ADD);
        testOperation(OperationType.SUB);
        testOperation(OperationType.MULT);
        testOperation(OperationType.DIV);
    }

    @Test
    public void testParse_BooleanOperations() {
        testBooleanOperation(BooleanOperationType.AND);
        testBooleanOperation(BooleanOperationType.OR);
    }

    private void testExpression(String expressionString, Consumer<Expression> checkCallback) {
        final ParseResult result = parser.parse(Collections.singletonList("{{" + expressionString + "}}"));

        assertEquals(1, result.getTemplate().getItems().size());

        final Display displayExpr = (Display) result.getTemplate().getItems().get(0);

        checkCallback.accept(displayExpr.getExpression());
    }

    private void testValue(String expressionString, Object value) {
        testExpression(expressionString, expression -> {
            assertTrue(expression instanceof Value);

            assertEquals(value, ((Value<?>) expression).getValue());
        });
    }

    private void testOperation(OperationType operationType) {
        final String exprString;

        switch (operationType) {
            case ADD:
                exprString = "10 + 2";
                break;
            case SUB:
                exprString = "10 - 2";
                break;
            case MULT:
                exprString = "10 * 2";
                break;
            case DIV:
                exprString = "10 / 2";
                break;
            default:
                throw new IllegalArgumentException("Unsupported operation type " + operationType);
        }

        testExpression(exprString, expression -> {
            final Operation op = (Operation) expression;

            assertEquals(operationType, op.getType());

            assertEquals(2, op.getExpressions().size());

            assertEquals(BigDecimal.valueOf(10), readValue(op.getExpressions().get(0)));
            assertEquals(BigDecimal.valueOf(2), readValue(op.getExpressions().get(1)));
        });
    }

    private void testBooleanOperation(BooleanOperationType operationType) {
        final String exprString;

        switch (operationType) {
            case AND:
                exprString = "true and false";
                break;
            case OR:
                exprString = "true or false";
                break;
            default:
                throw new IllegalArgumentException("Unsupported operation type " + operationType);
        }

        testExpression(exprString, expression -> {
            final BooleanOperation op = (BooleanOperation) expression;

            assertEquals(operationType, op.getType());

            assertEquals(2, op.getExpressions().size());

            assertEquals(Boolean.TRUE, readValue(op.getExpressions().get(0)));
            assertEquals(Boolean.FALSE, readValue(op.getExpressions().get(1)));
        });
    }

    private Object readValue(Expression expression) {
        assertTrue(expression instanceof Value<?>);

        return ((Value<?>) expression).getValue();
    }
}
