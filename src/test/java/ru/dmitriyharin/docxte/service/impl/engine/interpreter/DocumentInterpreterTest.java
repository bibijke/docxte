package ru.dmitriyharin.docxte.service.impl.engine.interpreter;

import org.docx4j.TextUtils;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.junit.jupiter.api.Test;
import ru.dmitriyharin.docxte.dto.engine.interpreter.context.Context;
import ru.dmitriyharin.docxte.dto.engine.interpreter.language.expression.identifier.Identifier;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DocumentInterpreterTest {
    private final DocumentInterpreter documentInterpreter = new DocumentInterpreter();

    @Test
    public void testRun() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("Paragraph of text");

        documentInterpreter.run(documentPart, new Context());

        assertEquals(1, documentPart.getContent().size());
        assertEquals("Paragraph of text", TextUtils.getText(documentPart.getContent().get(0)));
    }

    @Test
    public void testRun_BlockCondition() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("{% if true %}");
        documentPart.addParagraphOfText("Condition text");
        documentPart.addParagraphOfText("{% endif %}");

        documentInterpreter.run(documentPart, new Context());

        assertEquals(1, documentPart.getContent().size());
        assertEquals("Condition text", TextUtils.getText(documentPart.getContent().get(0)));
    }

    @Test
    public void testRun_InlineCondition() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("{% if true %}Inline condition text{% endif %}");

        documentInterpreter.run(documentPart, new Context());

        assertEquals(1, documentPart.getContent().size());
        assertEquals("Inline condition text", TextUtils.getText(documentPart.getContent().get(0)));
    }

    @Test
    public void testRun_BlockCycle() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("{% for key in keys loop loop1 %}");
        documentPart.addParagraphOfText("Key = {{ key }}");
        documentPart.addParagraphOfText("{% endfor %}");

        final Context context = new Context();
        context.getVariables().set(new Identifier("keys"), asList(1L, 2L));

        documentInterpreter.run(documentPart, context);

        assertEquals(2, documentPart.getContent().size());
        assertEquals("Key = 1.0", TextUtils.getText(documentPart.getContent().get(0)));
        assertEquals("Key = 2.0", TextUtils.getText(documentPart.getContent().get(1)));
    }

    @Test
    public void testRun_BlockInline() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("{% for key in keys %}{{ key }} {% endfor %}");

        final Context context = new Context();
        context.getVariables().set(new Identifier("keys"), asList(1L, 2L));

        documentInterpreter.run(documentPart, context);

        assertEquals(1, documentPart.getContent().size());
        assertEquals("1.0 2.0 ", TextUtils.getText(documentPart.getContent().get(0)));
    }

    @Test
    public void testRun_Display() throws Exception {
        final MainDocumentPart documentPart = new MainDocumentPart();
        documentPart.addParagraphOfText("{{ 3 }}");

        documentInterpreter.run(documentPart, new Context());

        assertEquals(1, documentPart.getContent().size());
        assertEquals("3", TextUtils.getText(documentPart.getContent().get(0)));
    }
}
