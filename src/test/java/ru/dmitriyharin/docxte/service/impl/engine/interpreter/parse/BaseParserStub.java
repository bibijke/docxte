package ru.dmitriyharin.docxte.service.impl.engine.interpreter.parse;

import ru.dmitriyharin.docxte.dto.engine.interpreter.language.Template;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseContext;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.ParseResult;
import ru.dmitriyharin.docxte.dto.engine.interpreter.parse.Scope;

import java.util.List;

public class BaseParserStub extends BaseParser<List<String>> {
    @Override
    public ParseResult parse(List<String> input) {
        final Template template = parse(String.join("\r\n", input));

        return new ParseResult(template, new ParseContext(Scope.DOCUMENT));
    }
}
